#/bin/ash

CLI_NAME=speedtest-cli

# Install dependancies
opkg install python-light
opkg install python-pip

# Install speedtest CLI script
wget -O $CLI_NAME.py https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py

# Optimize script by compiling all the modules it uses. 
# For more on this see: https://oldwiki.archive.openwrt.org/doc/software/python
python -m compileall

# Make the script executable and make it globally available
chmod +x $CLI_NAME.py
mv $CLI_NAME.py /bin/$CLI_NAME
