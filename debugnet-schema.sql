-- Configuration
CREATE TABLE Configuration(
  name			TEXT 		PRIMARY KEY 	NOT NULL,
  value			BLOB 		NOT NULL
);
INSERT INTO Configuration (
  name,
  value)
VALUES
  ('nmap_results_file_path', '/var/run/nmap.xml'), 	-- Nmap output path
  ('ip_scan_range', '192.168.8.1/24'),			-- IP range for Nmap to scan
  ('host_response_to_nmap_timeout', 60),		-- Time to wait before giving up on host response to nmap
  ('discovery_wait_between_scans', 60),			-- Seconds between network discovery scans
  ('nic_name_wifi_5_ghz', 'wlan0'),			-- Name of 5Ghz Wifi nic on router
  ('nic_name_wifi_2_4_ghz', 'wlan1'),			-- Name of 2.4Ghz Wifi nic on router
  ('byte_acc_period', 3),				-- Seconds to wait until byte counts are measured
  ('latency_wait_between_tests', 5),  			-- Seconds to wait between latency experiments
  ('signal_wait_between_measures', 2),			-- Time to wait between signal measuring
  ('bandwidth_wait_between_tests', 20);			-- Seconds to wait till next BW test

-- Device connection methods
CREATE TABLE ConnectionTypes(
  opt_id                INTEGER         PRIMARY KEY     NOT NULL,
  name                  TEXT            NOT NULL                 
); 
INSERT INTO ConnectionTypes (
  opt_id,
  name)
VALUES
  (1, 'conn_other'),
  (2, 'conn_none'),
  (3, 'conn_wifi_2_4'),
  (4, 'conn_wifi_5');

-- Device status options                                                                                              
CREATE TABLE DeviceStatus(                                                                                            
  opt_id                INTEGER         PRIMARY KEY     NOT NULL,                                                     
  name                  TEXT            NOT NULL 
);                                                                                                                    
INSERT INTO DeviceStatus (                                                                                            
  opt_id, 
  name)                                                                                                               
VALUES                                                                                                                
  (1, 'dev_offline'),
  (2, 'dev_online');  

-- Discovered Devices
CREATE TABLE Devices(
  mac_address		TEXT 		UNIQUE		NOT NULL,
  ip_address		TEXT,
  hostname		TEXT,
  manufacturer		TEXT,
  first_connected	INTEGER 	NOT NULL,
  last_connected	INTEGER 	NOT NULL,
  status		INTEGER 	NOT NULL,
  connection_type	INTEGER 	NOT NULL,
  FOREIGN KEY(status) REFERENCES DeviceStatus(opt_id)
  FOREIGN KEY(connection_type) REFERENCES ConnectionTypes(opt_id)
);

-- Metric tables
CREATE TABLE BandwidthISP(        
  time          	INTEGER 	NOT NULL,
  up	         	INTEGER 	NOT NULL,
  down	         	INTEGER 	NOT NULL
); 

CREATE TABLE LatencyDevices(
  time			INTEGER 	NOT NULL,
  device  		TEXT 		NOT NULL,
  max_latency		REAL,
  avg_latency		REAL,
  min_latency		REAL,
  loss			REAL		NOT NULL,
  FOREIGN KEY(device) REFERENCES Devices(mac_address)
);

CREATE TABLE LatencyISP(
  time          	INTEGER 	NOT NULL,
  latency          	INTEGER		NOT NULL
);

CREATE TABLE TrafficLoadDevices(        
  time          	INTEGER 	NOT NULL,
  device		TEXT 		NOT NULL,
  bps_in         	INTEGER 	NOT NULL,
  bps_out         	INTEGER 	NOT NULL,
  bps_total         	INTEGER 	NOT NULL,
  FOREIGN KEY(device) REFERENCES Devices(mac_address)
);

CREATE TABLE TrafficLoadEntNet(            
  time          	INTEGER 	NOT NULL,
  bps_in         	INTEGER 	NOT NULL,
  bps_out         	INTEGER 	NOT NULL,
  bps_total         	INTEGER 	NOT NULL
);

CREATE TABLE WirelessChannelUsage(        
  time          	INTEGER 	NOT NULL,
  value         	INTEGER 	NOT NULL
); 

CREATE TABLE WirelessSignal(
  time			INTEGER 	NOT NULL,
  device		TEXT 		NOT NULL,
  signal		INTEGER 	NOT NULL,
  FOREIGN KEY(device) REFERENCES Devices(mac_address)
);

