#!/bin/ash

DB_INSTALL_PATH="$HOME/debugnet"
DB_NAME=debugnet_db
SCHEMA_FILE_PATH=$(dirname "$0")
SCHEMA_FILE_NAME=debugnet-schema.sql

DB_FILE="$DB_INSTALL_PATH/$DB_NAME"
SCHEMA_FILE="$SCHEMA_FILE_PATH/$SCHEMA_FILE_NAME"

USAGE="
Create DebugNet database\n\n
usage: $(basename "$0") [-f] [-h]\n\n

Optional arguments:\n
\t    -f \t   delete and recreate database if it already exists\n
\t    -h \t   show this help
"

# Parse arguments
#while getopts h:f: opt; do
for i in "$@"
do
    case "$i" in
      -h) echo -e $USAGE; exit 0;;
      -f) FORCE_CREATION="true";;
    esac
done

# Remove DB file if it already exists, but only if the force flag
# is given.
if [ -f $DB_FILE ]
then
  if [ "$FORCE_CREATION" == "true" ]
  then
    echo "Removing existing database file at: $DB_FILE"
    rm $DB_FILE
  else
    echo "Database file already exists, use -f to recreate the database"
    exit 1
  fi
fi

echo "Creating database at: $DB_FILE"

sqlite3 $DB_FILE < $SCHEMA_FILE
