#/bin/ash


echo -e "DEBUGNET_SETUP: installing dependancies...\n"

# Resolve dependancies
opkg update
opkg install fping sqlite3-cli nmap
./$(dirname "$0")/setup-speedtest-client.sh
./$(dirname "$0")/setup-libpistache.sh

echo -e "\n\nDEBUGNET_SETUP: creating database...\n"

# Setup the database
./$(dirname "$0")/create_db.sh -f
