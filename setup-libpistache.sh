#/bin/ash

PISTACHE_PKG=$(dirname "$0")/pkgs/pistache
LIB_DIR=/usr/lib

# Transfer all pistache library files to library directory
cp -r $PISTACHE_PKG/* $LIB_DIR/.
